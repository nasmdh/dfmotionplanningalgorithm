//Dipole flow (DF) field for multiple agent path finding 
#include "ThetaPath.h"
#include "DFField.h"
#include "VelocityControl.h"
#include "Unities.h"
#include "time.h"
#include <curses.h>
// #include <conio.h>
#include <opencv2/opencv.hpp>
using namespace cv;
#define RUN_EXPERIMENT2
#define GRID_SIZE 2  //Step of the grid is 5, the higher value of this value, the faster processing time is
#define NUMBER_AGENT 4
#include <opencv2/core.hpp>

ThetaStar4Grid grid_graph_;
// clock_t total_clock_theta2flowforce = 0;

int main() {
	int TESTING_AGENT=2;
	int width = 500, height = 500; //map size
	Mat img(height,width, CV_8UC1, Scalar(0));

	Point agent_src[NUMBER_AGENT] = { Point(20,height / 2 - 5),
		Point(width / 2 - (width - 40) / (2 * sqrt(2)),height / 2 - (height - 40) / (2 * sqrt(2))),
		Point(width / 2 , 20),
		Point(width / 2 + (width - 40) / (2 * sqrt(2)),height / 2 - (height - 40) / (2 * sqrt(2))) };
	Point agent_dst[NUMBER_AGENT] = { Point(width - 20,height / 2 - 5),
		Point(width / 2 + (width - 40) / (2 * sqrt(2)),height / 2 + (height - 40) / (2 * sqrt(2))),
		Point(width / 2, height - 20),
		Point(width / 2 - (width - 40) / (2 * sqrt(2)),height / 2 + (height - 40) / (2 * sqrt(2))) };
	
	
	for(int i=0;i<NUMBER_AGENT;i++)
	line(img, agent_src[i], agent_dst[i], Scalar(255), 70, 8, 0);
	grid_graph_.SetGridStep(GRID_SIZE);
	
	Mat element = getStructuringElement(MORPH_ELLIPSE,
		Size(2 * AGENT_SIZE + 1, 2 * AGENT_SIZE + 1),
		Point(AGENT_SIZE, AGENT_SIZE));
	Mat img_graph;
	erode(img, img_graph, element);
	for (int i = 0; i < height; i++) {
		img_graph.at<uchar>(i, 0) = 0;
		img_graph.at<uchar>(i, width-1) = 0;
	}

	for (int i = 0; i < width; i++) {
		img_graph.at<uchar>(0, i) = 0;
		img_graph.at<uchar>(height-1, i) = 0;
	}

	grid_graph_.SetImage(img_graph);
	
	if (!grid_graph_.MakeGrid())
		printf("\n Cannot create the graph\n");

	int i;
	srand(time(NULL));
	width = img.cols;
	height = img.rows;
	uchar *ptr;
	ptr = img.data;
	cv::Mat color_img;
	cv::cvtColor(img, color_img, cv::COLOR_GRAY2BGR);
	DFField m_field;
	ThetaPath m_path[NUMBER_AGENT];

	//k=0.01 and n=10000 in equation (4)
	m_field.MakeRepusiveField(img, 100, 10000, 1.25);

	//Initialize a global path from source to destination
	for (i = 0;i < NUMBER_AGENT;i++) {
		m_path[i].setInitPath(&grid_graph_);
		m_path[i].InitFlowField(agent_src[i], agent_dst[i]);
		circle(color_img, agent_src[i], 5, Scalar(0, 255, 0), -1);
		circle(color_img, agent_dst[i], 5, Scalar(0, 255, 0), -1);
	}
	
	Point agent_[NUMBER_AGENT], pre_agent_[NUMBER_AGENT];
	VelocityControl velocity[NUMBER_AGENT];
	Point2f agent_velocity[NUMBER_AGENT], dipole_f12, dipole_f13, dipole_f14, dipole_f23, dipole_f24, dipole_f34;
	Scalar color_map[10] = { Scalar(90,60, 245),Scalar(125, 100, 65),Scalar(110, 40, 75), Scalar(180, 175, 0),Scalar(140, 125, 105),
		Scalar(120,100, 245),Scalar(175, 145, 105),Scalar(150, 85, 110), Scalar(205, 205, 80),Scalar(150, 160, 180) };

	char str[200];
	
	float dipole_ratio = 0;
	for (i = 0;i <NUMBER_AGENT;i++) {
		velocity[i].Init(agent_src[i], agent_dst[i], 10, 1 / DELTA_T);
		agent_[i] = agent_src[i];
		pre_agent_[i] = agent_[i];
		agent_velocity[i].x = 0;agent_velocity[i].y = 0;
		sprintf(str, "S%d", i + 1);
		if(i>1)
			putText(color_img, str, Point(agent_src[i].x, agent_src[i].y - 10), FONT_HERSHEY_PLAIN, 1, color_map[i]);
		else
			putText(color_img, str, Point(agent_src[i].x-20, agent_src[i].y + 20), FONT_HERSHEY_PLAIN, 1, color_map[i]);
		sprintf(str, "G%d", i + 1);
		putText(color_img, str, Point(agent_dst[i].x, agent_dst[i].y + 20), FONT_HERSHEY_PLAIN, 1, color_map[i]);
	}
	Mat color_img1 = color_img.clone();

	bool is_draw_line = false;
	for (i = 0;i < 400;i++) {
		Point2f df_force[NUMBER_AGENT];
		for (int j = 0;j < NUMBER_AGENT;j++) {
			if (i>3 )
				line(color_img, agent_[j], pre_agent_[j], color_map[j], 3, LINE_AA, 0);
				
			pre_agent_[j] = agent_[j];
			
			Point2f force_1 = m_path[j].Theta2FlowForce(Point2f(agent_[j].x, agent_[j].y));

			if (force_1.x == 0 && force_1.y == 0) {
				printf("No flow field, needs to reset\n");
				m_path[j].InitFlowField(agent_[j], agent_dst[j]);
				force_1 = m_path[j].Theta2FlowForce(Point2f(agent_[j].x, agent_[j].y));
			}
			Point2f force_2 = m_field.RepusiveForce(Point(agent_[j].x, agent_[j].y));
			Point2f static_force = force_1 + force_2;
			float s = static_force.x *static_force.x + static_force.y*static_force.y + 1e-12;
			s = sqrt(s);
			static_force.x /= s;static_force.x *= 10;
			static_force.y /= s;static_force.y *= 10;
			Mat img_field = m_field.Dipolefield(Point((int)(2 * agent_velocity[j].x), (int)(2 * agent_velocity[j].y)), Point(agent_[j].x, agent_[j].y), width, height);
			sprintf(str, "Dipole Field from Agent %d", j + 1);
			df_force[j] = static_force;
		}
		float s;
		int done = 0;
		for (int j = 0;j < NUMBER_AGENT;j++) {
			if ((abs(agent_[j].x - agent_dst[j].x) < 5 && abs(agent_[j].y - agent_dst[j].y) < 5)) {
				done++;
				continue;
			}
			if (!i)
				velocity[j].setTheta(atan2(df_force[j].y, df_force[j].x));
			else {
				velocity[j].Update(df_force[j]);
				agent_[j] = velocity[j].getPosition();
			}

				if (i % 5 == 0) {
					DrawAgent(color_img, velocity[j].getPosition(), velocity[j].getTheta(), color_map[j], i / 5, j + 1);
				}

		}
		if (done == NUMBER_AGENT)
			break;

		if (i % 5 == 0) {
			imshow("Multiple Agent Path Finding Without DF Field", color_img);
			cv::waitKey(50);
		}
	}


	VelocityControl velocity_planning[NUMBER_AGENT];
	dipole_ratio =	100;
	for (i = 0;i < NUMBER_AGENT;i++) {
		agent_[i] = agent_src[i];
		pre_agent_[i] = agent_[i];
		agent_velocity[i].x = 0;agent_velocity[i].y = 0;
		velocity_planning[i].Init(agent_src[i], agent_dst[i], 10, 1/DELTA_T);
	}

	for (i = 0;i < 400;i++) {
		Point2f df_force[NUMBER_AGENT];
		for (int j = 0;j <NUMBER_AGENT;j++) {
			if(i>3)
			line(color_img1, agent_[j], pre_agent_[j], color_map[j], 8, 8, 0);

			pre_agent_[j] = agent_[j];
			Point2f force_1 = m_path[j].Theta2FlowForce(Point2f(agent_[j].x, agent_[j].y));

			if (force_1.x == 0 && force_1.y == 0) {
				printf("No flow field, needs to reset\n");
				m_path[j].InitFlowField(agent_[j], agent_dst[j]);
				force_1 = m_path[j].Theta2FlowForce(Point2f(agent_[j].x, agent_[j].y));
			}

			Point2f force_2 = m_field.RepusiveForce(Point(agent_[j].x, agent_[j].y));
			Point2f static_force = force_1 + force_2;
			float s = static_force.x *static_force.x + static_force.y*static_force.y + 1e-12;
			s = sqrt(s);
			static_force.x /= s;static_force.x *= 10;
			static_force.y /= s;static_force.y *= 10;
			df_force[j] = static_force;
		}

		dipole_f12 = m_field.DipoleForce(agent_[0], agent_velocity[0], agent_[1], agent_velocity[1]);
		dipole_f13 = m_field.DipoleForce(agent_[0], agent_velocity[0], agent_[2], agent_velocity[2]);
		dipole_f14 = m_field.DipoleForce(agent_[0], agent_velocity[0], agent_[3], agent_velocity[3]);
		dipole_f23 = m_field.DipoleForce(agent_[1], agent_velocity[1], agent_[2], agent_velocity[2]);
		dipole_f24 = m_field.DipoleForce(agent_[1], agent_velocity[1], agent_[3], agent_velocity[3]);
		dipole_f34 = m_field.DipoleForce(agent_[2], agent_velocity[2], agent_[3], agent_velocity[3]);

		df_force[0] += dipole_ratio*(dipole_f12 + dipole_f13 + dipole_f14);
		df_force[1] += dipole_ratio*(dipole_f23 + dipole_f24 - dipole_f12);
		df_force[2] += dipole_ratio*(dipole_f34 - dipole_f23 - dipole_f13);
		df_force[3] += dipole_ratio*(-dipole_f34 - dipole_f24 - dipole_f14);
		float s;
		int done = 0;

		for (int j = 0;j < NUMBER_AGENT;j++) {
			if ((abs(agent_[j].x - agent_dst[j].x) < 5 && abs(agent_[j].y - agent_dst[j].y) < 5)) {
				done++;
				continue;
			}
			if(!i)
				velocity_planning[j].setTheta(atan2(df_force[j].y, df_force[j].x));
			else {
				agent_velocity[j] = velocity_planning[j].getLinearVelocity();
				velocity_planning[j].Update(df_force[j]);
				agent_[j] = velocity_planning[j].getPosition();
			}

	
			if (i % 5 == 0) {
					DrawAgent(color_img1, velocity_planning[j].getPosition(), velocity_planning[j].getTheta(), color_map[j], i / 5, j + 1);
					cv::waitKey(50
					
					);
			}
	
		}
		if (done == NUMBER_AGENT)
			break;
		imshow("Multiple Agent Path Finding With DF Field", color_img1);
	}

	std::cout << "total exec time::dipoleforce = " << DFField::total_clock_dipoleforce << std::endl;
	std::cout << "total exec time::repulsiveforce = " << DFField::total_clock_repulsiveforce << std::endl;
	std::cout << "total exec time::theta2flowforce = " << ThetaPath::total_clock_theta2flowforce << std::endl;

	waitKey();
	return -1;
}


