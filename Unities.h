#pragma once
#include <opencv2/opencv.hpp>
using namespace cv;
#define AGENT_SIZE 10
#define VECT_LEN 13
#define STR_INDEX 10
void DrawAgent(Mat img, Point pos, float theta, Scalar color);
void DrawAgent(Mat img, Point pos, float theta, Scalar color, float timeindex, int loc);
void DrawObject(Mat img, Point pos, float theta, Scalar color);
void DrawObject(Mat img, Point pos, float theta, Scalar color, float timeindex);