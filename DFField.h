#pragma once
#include <opencv2/opencv.hpp>
#include <ctime>
#define M_PI 3.14159265358979323846264338327950288
using namespace cv;
#define GAMA 1.0
class DFField
{
	Mat repulsive_field;
public:
	DFField();
	~DFField();
	void MakeRepusiveField(Mat obstacle_map, float scale, float gain, float dmax);
	Point2f RepusiveForce(Point loc);
	Mat Dipolefield(Point2d m, Point2d orig, int width, int height);
	Point2f DipoleForce(Point2f loc_agent, Point2f moment_agent, Point2f loc_obj, Point2f moment_obj);
	static clock_t total_clock_dipoleforce;
	static clock_t total_clock_repulsiveforce;
};

