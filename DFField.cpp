#include "DFField.h"
#include <hip/hip_runtime.h>

#define HIP_DIPOLFORCE
#define HIP_REPULSIVEFORCE

clock_t DFField::total_clock_repulsiveforce = 0;
clock_t DFField::total_clock_dipoleforce = 0;

__global__ void kernel_RepusiveForce (Point* loc, Mat* repulsive_field, Point2f* force_)
{
   unsigned gid = hipThreadIdx_x; // <- coordinate index function
   if (gid == 0) {
	   	int xt, yt;
		xt = loc->x;
		yt = loc->y;
		// Point2f force_;
		force_->x = -repulsive_field->at<float>(yt, xt + 1) + repulsive_field->at<float>(yt, xt);
		force_->y = -repulsive_field->at<float>(yt+1, xt) + repulsive_field->at<float>(yt, xt);
   }
}


DFField::DFField() {
}


DFField::~DFField() {
}


Mat DFField::Dipolefield(Point2d m, Point2d orig, int width, int height) {
	Mat dipolefield(height, width, CV_8UC1, Scalar(255));
	float i, j;
	float x0, y0, r, rx0, ry0, mx, my, fx, fy, r3;
	x0 = orig.x;
	y0 = orig.y;
	mx = m.x;
	my = m.y;
	// std::cout << "Dipolefield::width = " << width << std::endl;
	for (i = 0;i<width;i += 10)
		for (j = 0;j < height;j += 10) {
			if (orig.x != i || orig.y != j) {
				r = (i - x0)*(i - x0) + (j - y0)*(j - y0);
				rx0 = i - x0;
				ry0 = j - y0;
				r = sqrt(r);
				rx0 = rx0 / r;
				ry0 = ry0 / r;
				r3 = 4.0;
				fx = 3 * (mx*rx0 + my*ry0)*rx0 - mx;
				fy = 3 * (mx*rx0 + my*ry0)*ry0 - my;
				fx = fx / r3;
				fy = fy / r3;
				arrowedLine(dipolefield, Point(i, j), Point(i + fx, j + fy), Scalar(0), 1, 8, 0, 0.1);
			}
		}
	return dipolefield;
}


void DFField::MakeRepusiveField(Mat obstacle_map, float scale,float gain, float dmax) {
	int width, height, img_size;
	width = obstacle_map.cols;
	height = obstacle_map.rows;
	img_size = width*height;

	distanceTransform(obstacle_map, repulsive_field, DIST_L2, 3);
	float *ptr_repulsive, *ptr_attractive_field;
	ptr_repulsive = (float*)repulsive_field.data;
	float d, d0, d2;
	d0 = 1.0/dmax;
	for (int i = 0;i < img_size;i++, ptr_repulsive++) {
		d = *ptr_repulsive;
		d2 = d / scale + 1;
		d = (1 / d2 - d0); d = d*d;
		if (d2 <= dmax)
			*ptr_repulsive = gain*d;
		else
			*ptr_repulsive = 0;
	}

	double min_value;
	double max_value;
	minMaxIdx(repulsive_field, &min_value, &max_value);
	Mat adjMap;
	repulsive_field.clone().convertTo(adjMap, CV_8UC1, 255 / (max_value - min_value), -min_value);
	Mat adjMap_color;
	applyColorMap(adjMap, adjMap_color, COLORMAP_JET);
	//imshow("Repusive Field from Obstacles", adjMap_color);
}


Point2f DFField::RepusiveForce(Point loc) {
	clock_t start_clock = clock();

#if defined(HIP_REPULSIVEFORCE)
	Point2f* force_;
	Point* locBuffer;
	Point2f* forceBuffer;
	Mat* repulsive_fieldBuffer;

	// Device memory
	hipMalloc((void**)&locBuffer, sizeof(Point)) ;
    hipMalloc((void**)&forceBuffer, sizeof(Point2f));
	hipMalloc((void**)&repulsive_fieldBuffer, sizeof(Mat));
	// Host memory
	hipMalloc((void**)&force_, sizeof(Point2f));
	// Input: host to device
    hipMemcpy(locBuffer, &loc, sizeof(Point), hipMemcpyHostToDevice);
    hipMemcpy(repulsive_fieldBuffer, &repulsive_field, sizeof(Mat), hipMemcpyHostToDevice);
	// Call the kernel
	hipLaunchKernelGGL(kernel_RepusiveForce,
                  dim3(1),
                  dim3(1),
                  0, 0,
				  locBuffer, repulsive_fieldBuffer, forceBuffer );
	// Output: device to host
	hipMemcpy(force_, forceBuffer, sizeof(Point2f), hipMemcpyDeviceToHost);
	std::cout << "force: " << force_->x << " " << force_->y << std::endl;

    hipFree(locBuffer);
    hipFree(forceBuffer);
    hipFree(repulsive_fieldBuffer);
#else
	int xt, yt;
	xt = loc.x;
	yt = loc.y;
	Point2f *force_ = new Point2f; //malloc((void**)&force_, sizeof(Point2f));
	force_->x = -repulsive_field.at<float>(yt, xt + 1) + repulsive_field.at<float>(yt, xt);
	force_->y = -repulsive_field.at<float>(yt+1, xt) + repulsive_field.at<float>(yt, xt);
	// std::cout << "force: " << force_->x << " " << force_->y << std::endl;
#endif

	DFField::total_clock_repulsiveforce += clock()-start_clock;

	return *force_;
}

__global__ 
void kernel_DipoleForce(Point2f *loc_agent, Point2f *moment_agent, Point2f *loc_obj, Point2f *moment_obj,
							float2 *fxy) 
{
	unsigned gid = hipThreadIdx_x; // <- coordinate index function
	
   if (gid == 0) {
	float reversed_angles = M_PI + M_PI / 4;
	float xt, yt, obj_x, obj_y, mx, my;
	float fx = 0, fy = 0;
	float rx, ry, r2;

	mx = moment_agent->x;
	my = moment_agent->y;
	float m_obj_x = moment_obj->x;
	float m_obj_y = moment_obj->y;
	rx = loc_obj->x - loc_agent->x; //a vector from object to the agent
	ry = loc_obj->y - loc_agent->y;
	r2 = rx*rx + ry*ry+1e-12; //a small term 1e-12 is added into the denominator to avoid singularities
	rx = rx / sqrt(r2);
	ry = ry / sqrt(r2);

	fx = 0;
	fy = 0;

	fx += (m_obj_x*rx + m_obj_y*ry)*mx;
	fy += (m_obj_x*rx + m_obj_y*ry)*my;

	fx += (mx*rx + my*ry)*m_obj_x;
	fy += (mx*rx + my*ry)*m_obj_y;

	fx += (mx*m_obj_x + my*m_obj_y)*rx;
	fy += (mx*m_obj_x + my*m_obj_y)*ry;

	fx -= 5 * (mx*rx + my*ry)*(m_obj_x*rx + m_obj_y*ry)*rx;
	fy -= 5 * (mx*rx + my*ry)*(m_obj_x*rx + m_obj_y*ry)*ry;

	fx = 1000 * fx / powf(r2*r2, GAMA);
	fy = 1000 * fy / powf(r2*r2, GAMA);

	float sign = (fx*rx + fy*ry);

	if (sign>0) { //In some case it has an opposite direction of a
		//vector pointing from an agent k to an agent j, need to be reversed by rotation
		float new_fx = fx*cosf(reversed_angles)-fx*sinf(reversed_angles);
		float new_fy = fx*sinf(reversed_angles) + fx*cosf(reversed_angles);
		fx = new_fx;
		fy = new_fy;
	}
	fxy->x = fx;
	fxy->y = fy;
   }
}

Point2f DFField::DipoleForce(Point2f loc_agent, Point2f moment_agent, Point2f loc_obj, Point2f moment_obj) {
	clock_t start_clock = clock();
#if defined(HIP_DIPOLFORCE)
	float fx = 0, fy = 0;
	Point2f *loc_agent_buff, *moment_agent_buff, *loc_obj_buff, *moment_obj_buff;
	float2 *fxy_buff, *fxy_buff_;
	// Device memory
	hipMalloc((void**)&loc_agent_buff, sizeof(Point2f)) ;
    hipMalloc((void**)&moment_agent_buff, sizeof(Point2f));
	hipMalloc((void**)&loc_obj_buff, sizeof(Point2f));
	hipMalloc((void**)&moment_obj_buff, sizeof(Point2f));
	hipMalloc((void**)&fxy_buff, sizeof(float2));
	// Host memory
	hipMalloc((void**)&fxy_buff_, sizeof(float2));
	// Input: host to device
    hipMemcpy(loc_agent_buff, &loc_agent, sizeof(Point2f), hipMemcpyHostToDevice);
    hipMemcpy(moment_agent_buff, &moment_agent, sizeof(Point2f), hipMemcpyHostToDevice);
    hipMemcpy(loc_obj_buff, &loc_obj, sizeof(Point2f), hipMemcpyHostToDevice);
    hipMemcpy(moment_obj_buff, &moment_obj, sizeof(Point2f), hipMemcpyHostToDevice);
	// Call the kernel
	hipLaunchKernelGGL(kernel_DipoleForce,
                  dim3(1),
                  dim3(1),
                  0, 0,
				  loc_agent_buff, moment_agent_buff, loc_obj_buff, moment_obj_buff, fxy_buff);
	// Output: device to host
	hipMemcpy(fxy_buff_, fxy_buff, sizeof(float2), hipMemcpyDeviceToHost);
	fx = fxy_buff_->x;
	fy = fxy_buff_->y;
	// std::cout << "fx =  " << fx.x << " " << force_->y << std::endl;

    hipFree(loc_agent_buff);
    hipFree(moment_agent_buff);
    hipFree(loc_obj_buff);
    hipFree(moment_obj_buff);
    hipFree(fxy_buff);
    hipFree(fxy_buff_);
#else
	float reversed_angles = M_PI + M_PI / 4;
	float xt, yt, obj_x, obj_y, mx, my;
	float fx = 0, fy = 0;
	float rx, ry, r2;
	mx = moment_agent.x;
	my = moment_agent.y;
	float m_obj_x = moment_obj.x;
	float m_obj_y = moment_obj.y;
	rx = loc_obj.x - loc_agent.x; //a vector from object to the agent
	ry = loc_obj.y - loc_agent.y;
	r2 = rx*rx + ry*ry+1e-12; //a small term 1e-12 is added into the denominator to avoid singularities
	rx = rx / sqrt(r2);
	ry = ry / sqrt(r2);

	fx = 0;
	fy = 0;

	fx += (m_obj_x*rx + m_obj_y*ry)*mx;
	fy += (m_obj_x*rx + m_obj_y*ry)*my;

	fx += (mx*rx + my*ry)*m_obj_x;
	fy += (mx*rx + my*ry)*m_obj_y;

	fx += (mx*m_obj_x + my*m_obj_y)*rx;
	fy += (mx*m_obj_x + my*m_obj_y)*ry;

	fx -= 5 * (mx*rx + my*ry)*(m_obj_x*rx + m_obj_y*ry)*rx;
	fy -= 5 * (mx*rx + my*ry)*(m_obj_x*rx + m_obj_y*ry)*ry;

	fx = 1000 * fx / pow(r2*r2, GAMA);
	fy = 1000 * fy / pow(r2*r2, GAMA);

	float sign = (fx*rx + fy*ry);

	if (sign>0) { //In some case it has an opposite direction of a
		//vector pointing from an agent k to an agent j, need to be reversed by rotation
		float new_fx = fx*cos(reversed_angles)-fx*sin(reversed_angles);
		float new_fy = fx*sin(reversed_angles) + fx*cos(reversed_angles);
		fx = new_fx;
		fy = new_fy;
	}

#endif

	DFField::total_clock_dipoleforce += clock()-start_clock;

	return Point2f(fx, fy);
}

