#include "Unities.h"

void DrawAgent(Mat img, Point pos, float theta, Scalar color) {
	circle(img, pos, AGENT_SIZE, color, 1, 8, 0);
	int dx, dy, x, y, fx, fy;
	dx = VECT_LEN*cos(theta);
	dy = VECT_LEN*sin(theta);
	fx = AGENT_SIZE*sin(theta) * 3 / 4;
	fy = -AGENT_SIZE*cos(theta) * 3 / 4;
	x = pos.x;y = pos.y;
	arrowedLine(img, pos, Point(x + dx, y + dy), color, 1, 8, 0, 0.1);
	dx = dx / 4;dy = dy / 4;
	line(img, Point(x + fx - dx, y + fy - dy), Point(x + fx + dx, y + fy + dy), color, 4, 8, 0);
	fx = -AGENT_SIZE*sin(theta) * 3 / 4;
	fy = AGENT_SIZE*cos(theta) * 3 / 4;
	line(img, Point(x + fx - dx, y + fy - dy), Point(x + fx + dx, y + fy + dy), color, 4, 8, 0);
}

void DrawAgent(Mat img, Point pos, float theta, Scalar color, float timeindex, int loc) {
	circle(img, pos, AGENT_SIZE, color, 1, 8, 0);
	int dx, dy, x, y, fx, fy;
	dx = VECT_LEN*cos(theta);
	dy = VECT_LEN*sin(theta);
	fx = AGENT_SIZE*sin(theta) * 3 / 4;
	fy = -AGENT_SIZE*cos(theta) * 3 / 4;
	x = pos.x;y = pos.y;
	arrowedLine(img, pos, Point(x + dx, y + dy), color, 1, 8, 0, 0.1);
	dx = dx / 4;dy = dy / 4;
	line(img, Point(x + fx - dx, y + fy - dy), Point(x + fx + dx, y + fy + dy), color, 4, 8, 0);
	fx = -AGENT_SIZE*sin(theta) * 3 / 4;
	fy = AGENT_SIZE*cos(theta) * 3 / 4;
	line(img, Point(x + fx - dx, y + fy - dy), Point(x + fx + dx, y + fy + dy), color, 4, 8, 0);
	char str[STR_INDEX];
	sprintf(str, "%.0f", timeindex);
	switch (loc) {
	case 1:
	case 2:
		putText(img, str, pos + Point(0, -10), FONT_HERSHEY_PLAIN, .8, color);
		break;
	case 3:
	case 4:
		putText(img, str, pos + Point(10, 10), FONT_HERSHEY_PLAIN, .8, color);
		break;

	}
}

void DrawObject(Mat img, Point pos, float theta, Scalar color) {
	circle(img, pos, AGENT_SIZE, color, 1, 8, 0);
	int dx, dy, x, y, fx, fy;
	dx = 1.2*AGENT_SIZE*cos(theta);
	dy = 1.2*AGENT_SIZE*sin(theta);
	fx = AGENT_SIZE*sin(theta);
	fy = -AGENT_SIZE*cos(theta);
	x = pos.x;y = pos.y;
	line(img, Point(x + fx - dx, y + fy - dy), Point(x + dx, y + dy), color, 1, 8, 0);
	line(img, Point(x + fx - dx, y + fy - dy), Point(x - dx / 4, y - dy / 4), color, 1, 8, 0);
	fx = -AGENT_SIZE*sin(theta);
	fy = AGENT_SIZE*cos(theta);
	line(img, Point(x + fx - dx, y + fy - dy), Point(x + dx, y + dy), color, 1, 8, 0);
	line(img, Point(x + fx - dx, y + fy - dy), Point(x - dx / 4, y - dy / 4), color, 1, 8, 0);
}


void DrawObject(Mat img, Point pos, float theta, Scalar color, float timeindex) {
	circle(img, pos, AGENT_SIZE, color, 1, 8, 0);
	int dx, dy, x, y, fx, fy;
	dx = 1.2*AGENT_SIZE*cos(theta);
	dy = 1.2*AGENT_SIZE*sin(theta);
	fx = AGENT_SIZE*sin(theta);
	fy = -AGENT_SIZE*cos(theta);
	x = pos.x;y = pos.y;
	line(img, Point(x + fx - dx, y + fy - dy), Point(x + dx, y + dy), color, 1, 8, 0);
	line(img, Point(x + fx - dx, y + fy - dy), Point(x - dx / 4, y - dy / 4), color, 1, 8, 0);
	fx = -AGENT_SIZE*sin(theta);
	fy = AGENT_SIZE*cos(theta);
	line(img, Point(x + fx - dx, y + fy - dy), Point(x + dx, y + dy), color, 1, 8, 0);
	line(img, Point(x + fx - dx, y + fy - dy), Point(x - dx / 4, y - dy / 4), color, 1, 8, 0);
	char str[STR_INDEX];
	sprintf(str, "%.2f", timeindex);
	putText(img, str, pos - Point(10, 20), FONT_HERSHEY_PLAIN, 1, color);
}