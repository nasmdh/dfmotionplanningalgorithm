// #include "conio.h"
#include "VelocityControl.h"

VelocityControl::VelocityControl() {
}


VelocityControl::~VelocityControl() {
}

void VelocityControl::Update(Point2f force) {
	float u, omega;
	float d = (pos.x - goal.x)*(pos.x - goal.x) + (pos.y - goal.y)*(pos.y - goal.y);
	d = sqrt(d) / 20;
	u = k_u*tanh(d);
	omega = -k_omega*(theta - atan2(force.y, force.x));
	pos.x = pos.x + (int)(u*DELTA_T*cos(theta));
	pos.y = pos.y + (int)(u*DELTA_T*sin(theta));
	theta = theta + omega*DELTA_T;
	linear_velocity = Point(u*cos(theta), u*sin(theta));
	angular_velocity = omega;
}


Point VelocityControl::getPosition() {
	return pos;
}

float VelocityControl::getTheta() {
	return theta;
}

Point VelocityControl::getLinearVelocity() {
	return linear_velocity;
}

void VelocityControl::setTheta(float value) {
	theta = value;
}

