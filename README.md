## Dipole Flow Field
Dipole Field is a motion control algorithm developed by Lan Anh et al. (Dipole Flow Field for Dependable Path Planning of Multiple Agents). It directs an agent (e.g a robot) by avoiding static and dynamic obstacles on the path from source to target (or goal). It uses Teta* for its path planning algorithm. The Dipole Flow Field algorithm is based on the *Dipole* concept from electromagnetism which computes attractive and repulsive forces between objects to make optimal next move from the current position.

The algorithm is implemented to run on both CPU and GPU. It is implemented using C++ Heterogeneous-Compute Interface for Portability (HIP). Make sure the following preprocessing directives are uncommented so that respective parts of the code are compiled for the GPU.

-    #define HIP_THETA2FLOWFORCE
-    #define HIP_DIPOLFORCE
-    #define HIP_REPULSIVEFORCE
  

### Compilation
Simply execute the make file as `# make`

### Demo
There are six agents moving towards an intersection. In this case, the Dipole Field Forcee algorithm should be able to avoid the collision. It is demonstrated graphically as can be seen by executing the command `# .\dip` from the root directory.

### Desclaimer
The algorithm implementation is from Lan et al. My role was to port some parts of the algorithm to GPU.

### Contact
Nesredin Mahmud <nesredin.mahmud@gmail.com>