FLAGS=-I/usr/local/include/opencv2/
LIBS=-lopencv_core -lopencv_imgproc -lopencv_highgui
HIP_PATH?= $(wildcard /opt/rocm/hip)
ifeq (,$(HIP_PATH))
	HIP_PATH=../../..
endif

# HIPCC=g++
HIPCC=$(HIP_PATH)/bin/hipcc

TARGET=hcc

SOURCES = main.cpp DFField.cpp VelocityControl.cpp ThetaPath.cpp MinSearchHeap.cpp ThetaStar4Grid.cpp Unities.cpp
DEPS = DFField.h ThetaPath.h MinSearchHeap.h ThetaStar4Grid.h Unities.h VelocityControl.h
OBJECTS = $(SOURCES:.cpp=.o)

EXECUTABLE=./dip

.PHONY: test


all: $(EXECUTABLE) test

CXXFLAGS =-g
CXX=$(HIPCC)


$(EXECUTABLE): $(OBJECTS) $(DEPS)
	$(HIPCC) $(OBJECTS) -o $@  $(FLAGS) $(LIBS)


test: $(EXECUTABLE)
	$(EXECUTABLE)


clean:
	rm -f $(EXECUTABLE)
	rm -f $(OBJECTS)
	rm -f $(HIP_PATH)/src/*.o


