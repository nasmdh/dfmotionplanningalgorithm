#pragma once
#include <opencv2/opencv.hpp>
using namespace cv;
#define DELTA_T 1   

class VelocityControl
{
	Point goal;
	float k_u, k_omega,theta;
	Point pos;
	Point linear_velocity;
	float angular_velocity;
	float integral_error, diff_error,previous_error;

public:
	VelocityControl();
	void Init(Point s, Point g, float k1, float k2) {
		goal = g;
		k_u = k1;
		k_omega = k2;
		theta = 0;
		pos = s;
		linear_velocity = Point(0,0);
		angular_velocity = 0;

		integral_error = 0;
		diff_error = 0; 
		previous_error = 0;
	}

	~VelocityControl();
	void setTheta(float value);
	void Update(Point2f force);
	Point getPosition();
	float getTheta();
	Point getLinearVelocity();
};

