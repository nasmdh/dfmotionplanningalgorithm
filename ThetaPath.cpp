#include "ThetaPath.h"
#include <hip/hip_runtime.h>

#define HIP_THETA2FLOWFORCE

clock_t ThetaPath::total_clock_theta2flowforce = 0;

ThetaPath::ThetaPath() {
	x = NULL;y = NULL;nx = NULL;ny = NULL;
	segment_len = NULL;
	num_of_segment = 0;
}

void ThetaPath::BuildPath(vector<Point> path_) {
	float s;
	num_of_segment = path_.size()-1;
	x = new float[num_of_segment];
	y = new float[num_of_segment];
	nx = new float[num_of_segment];
	ny = new float[num_of_segment];
	segment_len = new float[num_of_segment];
	std::reverse(path_.begin(), path_.end());
	end.x = (float)path_[num_of_segment].x;
	end.y = (float)path_[num_of_segment].y;

std::cout << "num_of_segment = " << num_of_segment << std::endl;

	for (int i = 0; i < num_of_segment; ++i) {
		x[i] = (float)path_[i].x;
		y[i] = (float)path_[i].y;
		nx[i]= (float)path_[i+1].x- (float)path_[i].x;
		ny[i] = (float)path_[i + 1].y - (float)path_[i].y;
		s = nx[i] * nx[i] + ny[i] * ny[i];
		s = sqrt(s);
		segment_len[i] = s;
		nx[i] /= s;
		ny[i] /= s;
	}
}

__global__ 
void kernel_Theta2FlowForce(Point2f *s, Point2f *end,
				float *segment_len, float *x, float *y, float *nx, float *ny,
				int num_of_segment,
				Point2f *return_force_)
{
	unsigned i = hipThreadIdx_x + hipBlockIdx_x*hipBlockDim_x; // <- coordinate index function
	if(i < num_of_segment)
	{
		float min_dist, dist, tmp, alpha;
		Point2f force, return_force, follow_force;
		__shared__ float min_dist_arr[10];
		__shared__ float min_dist_shared;
		__shared__ int min_index;

		min_dist = sqrt((s->x - end->x)*(s->x - end->x) + (s->y - end->y)*(s->y - end->y)) + 1e-12;
		return_force.x = -(s->x - end->x) / min_dist;
		return_force.y = -(s->y - end->y) / min_dist;
		follow_force = return_force;
		*return_force_ = return_force;

		force.x = x[i] - s->x;
		force.y = y[i] - s->y;
		tmp = force.x*nx[i] + force.y*ny[i];
		force.x = force.x - tmp*nx[i];
		force.y = force.y - tmp*ny[i];
		dist = sqrt(force.x*force.x + force.y*force.y)+1e-12;
		if (dist < min_dist && tmp<=0 && abs(tmp)<=segment_len[i]) {
			min_dist_arr[i] = dist;
			return_force.x = force.x / dist;
			return_force.y = force.y / dist;
			follow_force.x = nx[i];
			follow_force.y = ny[i];
		}

		dist = sqrt((s->x - x[i])*(s->x - x[i]) + (s->y - y[i])*(s->y - y[i])) + 1e-12;
		if (dist < min_dist) {
			return_force.x = -(s->x - x[i]) / dist;
			return_force.y = -(s->y - y[i]) / dist;
			follow_force.x = nx[i];
			follow_force.y = ny[i];
			min_dist_arr[i] = dist;
		}
		__syncthreads();


		if(i == 0)
		{
			min_dist_shared = min_dist_arr[0];
			for(int j = 0; j < num_of_segment; ++j)
			{
				if(min_dist_arr[j] < min_dist)
				{
					min_index = j;
					min_dist_shared = min_dist_arr[j];
				}
			}
		}
		__syncthreads();	
		if(i == min_index)
		{
			if (min_dist_shared < FLOWFIELD_SCOPE) {
				float k1 = 1.0 / 100,k2=1.0;
				alpha = exp(-min_dist_shared *k1);
				return_force = (1 - alpha)*return_force + k2*alpha*follow_force;
			}
			else {
				return_force.x = 0;
				return_force.y = 0;
			}

			*return_force_ = return_force;
		}

	}
}

Point2f ThetaPath:: Theta2FlowForce(Point2f s) {

	clock_t start_clock = clock();

	
	std::cout << "num_of_segment = " << num_of_segment << std::endl;
		
#if defined(HIP_THETA2FLOWFORCE)
	Point2f return_force;
	Point2f *return_force_out;
	Point2f *s_in, *end_in;
	float *x_in, *y_in, *segment_len_in, *nx_in, *ny_in;

	//Host memory
	// return_force = (Point2f*) malloc(sizeof(Point2f));	
	// CHECK_ALLOCATION(return_force, "Failed to allocate host memory. (return_force)");

	// Input 
	hipMalloc((void**)&s_in, sizeof(Point2f)) ;
	hipMalloc((void**)&end_in, sizeof(Point2f)) ;
    hipMalloc((void**)&x_in, num_of_segment*sizeof(float));
	hipMalloc((void**)&y_in, num_of_segment*sizeof(float));
	hipMalloc((void**)&nx_in, num_of_segment*sizeof(float));
	hipMalloc((void**)&ny_in, num_of_segment*sizeof(float));
	hipMalloc((void**)&segment_len_in, num_of_segment*sizeof(float));

	// Output
	hipMalloc((void**)&return_force_out, sizeof(Point2f)) ;
	
	// Copy: host to device
    hipMemcpy(s_in, &s, sizeof(Point2f), hipMemcpyHostToDevice);
    hipMemcpy(end_in, &end, sizeof(Point2f), hipMemcpyHostToDevice);
    hipMemcpy(segment_len_in, &segment_len, num_of_segment*sizeof(float), hipMemcpyHostToDevice);
    hipMemcpy(x_in, x, num_of_segment*sizeof(float), hipMemcpyHostToDevice);
    hipMemcpy(y_in, y, num_of_segment*sizeof(float), hipMemcpyHostToDevice);
    hipMemcpy(nx_in, nx, num_of_segment*sizeof(float), hipMemcpyHostToDevice);
    hipMemcpy(ny_in, ny, num_of_segment*sizeof(float), hipMemcpyHostToDevice);
	
	// Call the kernel
	hipLaunchKernelGGL(kernel_Theta2FlowForce,
                  dim3(1),
                  dim3(num_of_segment),
                  0, 0,
				  s_in, end_in, segment_len_in, x_in, y_in, nx_in, ny_in, 
				  num_of_segment, 
				  return_force_out);

	// Copy: device to host
	hipMemcpy(&return_force, return_force_out, sizeof(Point2f), hipMemcpyDeviceToHost);

    // Input
	hipFree(s_in);
	hipFree(end_in);
    hipFree(x_in);
    hipFree(y_in);
    hipFree(nx_in);
    hipFree(ny_in);
	hipFree(segment_len_in);

	ThetaPath::total_clock_theta2flowforce += clock()-start_clock;

	return return_force;

#else
	float min_dist, dist, tmp, alpha;
	Point2f force, return_force, follow_force;

	min_dist = sqrt((s.x - end.x)*(s.x - end.x) + (s.y - end.y)*(s.y - end.y)) + 1e-12;
	return_force.x = -(s.x - end.x) / min_dist;
	return_force.y = -(s.y - end.y) / min_dist;
	follow_force = return_force;
	
	for (int i = 0; i < num_of_segment; ++i) {

		force.x = x[i] - s.x;
		force.y = y[i] - s.y;
		tmp = force.x*nx[i] + force.y*ny[i];
		force.x = force.x - tmp*nx[i];
		force.y = force.y - tmp*ny[i];
		dist = sqrt(force.x*force.x + force.y*force.y)+1e-12;
		if (dist < min_dist && tmp<=0 && abs(tmp)<=segment_len[i]) {
			min_dist = dist;
			return_force.x = force.x / dist;
			return_force.y = force.y / dist;
			follow_force.x = nx[i];
			follow_force.y = ny[i];
		}

		dist = sqrt((s.x - x[i])*(s.x - x[i]) + (s.y - y[i])*(s.y - y[i])) + 1e-12;
		if (dist < min_dist) {
			return_force.x = -(s.x - x[i]) / dist;
			return_force.y = -(s.y - y[i]) / dist;
			follow_force.x = nx[i];
			follow_force.y = ny[i];
			min_dist = dist;
		}

	}

	if (min_dist < FLOWFIELD_SCOPE) {
		float k1 = 1.0 / 100,k2=1.0;
		alpha = exp(-min_dist *k1);
		return_force = (1 - alpha)*return_force + k2*alpha*follow_force;
	}
	else {
		return_force.x = 0;
		return_force.y = 0;
	}

	ThetaPath::total_clock_theta2flowforce += clock()-start_clock;

	return return_force;
	
	#endif

}

ThetaPath::~ThetaPath() {
	if (x != NULL) delete x;
	if (y != NULL) delete y;
	if (nx != NULL) delete nx;
	if (ny != NULL) delete ny;
	if (segment_len != NULL) delete segment_len;
	num_of_segment = 0;
}

void ThetaPath::setInitPath(ThetaStar4Grid *g) {
	grid_graph_ = g;
}

void ThetaPath::InitFlowField(Point s, Point d) {
	if (x != NULL) delete x;
	if (y != NULL) delete y;
	if (nx != NULL) delete nx;
	if (ny != NULL) delete ny;
	if (segment_len != NULL) delete segment_len;
	num_of_segment = 0;

	int src, dst;
	src = grid_graph_->GetNodeIndex(s.x, s.y);
	dst = grid_graph_->GetNodeIndex(d.x, d.y);
	grid_graph_->Theta(src, dst);
	int x,y,u = dst;
	vector<Point> shortest_path;
	Point current;
	grid_graph_->GetNodeCoordinate(u, x, y);
	current.x = x;current.y = y;
	shortest_path.push_back(current);

	while (u != src && grid_graph_->g_score_[u] != INF) {
		grid_graph_->GetNodeCoordinate(grid_graph_->parent_list_[u],x,y);
		current.x = x;current.y = y;
		shortest_path.push_back(current);
		u = grid_graph_->parent_list_[u];
	}
	BuildPath(shortest_path);

}
