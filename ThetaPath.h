#pragma once
#include <opencv2/opencv.hpp>
#include "ThetaStar4Grid.h"
using namespace cv;
#define FLOWFIELD_SCOPE 20
class ThetaPath
{
	ThetaStar4Grid *grid_graph_;
public:
	float *x, *y;
	float *nx, *ny;
	float *segment_len;
	Point2f end;
	int num_of_segment;
	static clock_t total_clock_theta2flowforce;
	ThetaPath();
	void BuildPath(vector<Point> path_);
	Point2f  Theta2FlowForce(Point2f s);
	void setInitPath(ThetaStar4Grid *g);
	void InitFlowField(Point s,Point d);
	~ThetaPath();
};

 